let tracks = [
    {url : "./songs/Dargen D'Amico - Dove Si Balla (Sanremo 2022).mp3" , cover:"./img_music/cover-1.jpg" , artist:"Dargen D'Amico" , title:"Dove Si Balla"},
    {url : "./songs/Tommaso Paradiso - Tutte le notti.mp3" , cover:"./img_music/cover-2.jpg" , artist:"Tommaso Paradiso" , title:"Tutte le notti"},
    {url : "./songs/883 - Ti sento vivere (Official Video).mp3" , cover:"./img_music/cover-4.jpg" , artist:"883" , title:"Ti sento vivere"},
    {url : "./songs/Max Pezzali - L'universo tranne noi [Official Lyric Video].mp3" , cover:"./img_music/cover-5.jpg" , artist:"Max Pezzali" , title:"L'universo tranne noi"},
    {url : "./songs/Eros Ramazzotti - La Cosa Mas Bella (Più Bella Cosa) (Official Video).mp3" , cover:"./img_music/cover-6.jpg" , artist:"Ramazzotti" , title:"La Cosa Mas Bella"},
    {url : "./songs/LDA Quello che fa male -Testo.mp3" , cover:"./img_music/cover-8.jpg" , artist:"LDA" , title:"Quello che fa male"},
    {url : "./songs/Boomdabash - Tropicana (Lyric Video).mp3" , cover:"./img_music/cover-9.jpg" , artist:"Boomdabash" , title:"Tropicana"},
    {url : "./songs/Fedez - LA DOLCE VITA (Official Video).mp3" , cover:"./img_music/cover-10.jpg" , artist:"Fedez" , title:"La dolce vita"},
]

let track = document.querySelector('#track');

let btnPlay = document.querySelector('#btn-play');
let btnPause = document.querySelector('#btn-pause');
let btnPrev = document.querySelector('#btn-prev');
let btnNext = document.querySelector('#btn-next');
let sidebarToggler = document.querySelector('#sidebar-toggler');

let trackArtist = document.querySelector('#artist-track');
let trackTitle = document.querySelector('#title-track');
let trackCover = document.querySelector('#cover-track');

let currentTime = document.querySelector('#current-time');
let totalTime = document.querySelector('#total-time');

let playing = false;
let trackcurr = 0;

//funzione per avviare / mettere in pausa una canzone
function play() {
    if(!playing){
        btnPlay.classList.add('d-none');
        btnPause.classList.remove('d-none');
        trackCover.classList.add('active');
        track.play();
        playing = true;
    } else {
        btnPlay.classList.remove('d-none');
        btnPause.classList.add('d-none');
        trackCover.classList.remove('active');
        track.pause();
        playing = false;
    }
}

//funzione per cambiare una canzone successiva
function next(){
    trackcurr++;

    if (trackcurr > tracks.length - 1) {
        trackcurr = 0;
    }
    //console.log(tracks[trackcurr]);
    changeTrackDetails();
    changePlaylistActive();

    // console.log(trackArtist);
    // console.log(trackTitle);

    if (playing) {
        playing = false;
        play();
    } 
}

//funzione per cambiare una canzone precedente
function prev(){
    trackcurr--;

    if (trackcurr < 0) {
        trackcurr = tracks.length - 1;
    }
    //console.log(tracks[trackcurr]);
    
    changeTrackDetails();
    changePlaylistActive();

    // console.log(trackArtist);
    // console.log(trackTitle);

    if (playing) {
        playing = false;
        play();
    } 
}

//funzione per la sidebar 
function openSidebar() {
    let sidebar = document.querySelector('#sidebarmusic');
    sidebar.classList.toggle('open');
}

function changeTrackDetails(){
    track.src = tracks[trackcurr].url;
    trackArtist.innerHTML = tracks[trackcurr].artist;
    trackTitle.innerHTML = tracks[trackcurr].title;
    trackCover.src = tracks[trackcurr].cover;
}

function changePlaylistActive() {
    let trackListCard = document.querySelectorAll('.track-card');

    trackListCard.forEach((card,index) => {
        if (index == trackcurr) {
            card.classList.add('active');
        } else {
            card.classList.remove('active');
        }
    })
}

function populateTracklist() {
    let allList = document.querySelector('#tracklist');

    tracks.forEach((track, index)=> {
        let card = document.createElement('div');
        card.classList.add('col-12');
        card.innerHTML =
        `
        <div class="d-flex align-items-center justify-content-between px-4 py-3 border-main track-card">
            <img class="img_size" src="${track.cover}" alt="${track.title}">
            <div class="text-white">
                <h5 class="mb-0 tx-gradient">${track.artist}</h5>
                <p class="mb-0">${track.title}</p>
            </div>
            <i data-track="${index}" class="fa-solid fa-headphones-simple fs-3 tx-gradient pointer playlist-play"></i>
        </div>
        `
        allList.appendChild(card);
    })
    let btnsPlay = document.querySelectorAll('.playlist-play');

     btnsPlay.forEach(btn=> {
        btn.addEventListener('click', function() {
            let selectedTrack = btn.getAttribute('data-track');
            trackcurr = selectedTrack;
            
            changeTrackDetails();
            changePlaylistActive();

            if (playing) {
                playing=false;
                play();
            }
            
            //console.log(trackcurr);
            //console.log(btn.parentNode);

            
        })
     })
}

btnPlay.addEventListener('click', play);
btnPause.addEventListener('click', play);
btnNext.addEventListener('click',next);
btnPrev.addEventListener('click',prev);
track.addEventListener('ended',next);
sidebarToggler.addEventListener('click', openSidebar);

setInterval(function(){
    currentTime.innerHTML = formatTime(track.currentTime);
    totalTime.innerHTML = formatTime(track.duration);
},900)

function formatTime(sec) {
    let minutes = Math.floor(sec/60);
    let seconds = Math.floor(sec-minutes*60);
    if (seconds < 10) {
        seconds = `0${seconds}`;
    }
    return `${minutes}.${seconds}`;
}

populateTracklist();
changeTrackDetails();
changePlaylistActive();
